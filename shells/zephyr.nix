{ pkgs ? import ../default.nix }:

let
  python-packages = pkgs.python3.withPackages(p: with p; [
    pyelftools
    pyyaml
    pykwalify
    canopen
    packaging
    progress
    psutil
    anytree
    intelhex
    west

    cryptography
    intelhex
    click
    cbor

    protobuf
    setuptools

    # ci
    junitparser
    python_magic
    tabulate
    ply
  ]);
  packages = with pkgs; [
    gcc-arm-embedded
    cmake
    ninja
    gperf
    python3
    ccache
    qemu
    dtc
    (python-packages)

    # for bossa bootloaders
    bossa

    #stlink
    #openocd

    # ci
    #gitlint

    # For mcuboot development, we want both Rust and go.
    #go
    #openssl.dev
    #pkgconfig
    #sqlite.dev
    #cargo

    # native posix nng tests
    glibc_multi
    gcc_multi

    protobuf
  ];
in
pkgs.mkShell {
  name = "zephyr";

  buildInputs = packages;
  shellHook = ''
    export ZEPHYR_TOOLCHAIN_VARIANT=gnuarmemb
    export GNUARMEMB_TOOLCHAIN_PATH=${pkgs.gcc-arm-embedded}
    source ./zephyr/zephyr-env.sh
  '';
}

